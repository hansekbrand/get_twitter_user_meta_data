/* slurp.c is based on work made by Daniel Stenberg (getinmemory), Alexander Meyer (slurp), and Yaroslav Stavnichiy (nxjson) */

/* https://curl.haxx.se/libcurl/c/getinmemory.html */
/***************************************************************************
 *                                  _   _ ____  _
 *  Project                     ___| | | |  _ \| |
 *                             / __| | | | |_) | |
 *                            | (__| |_| |  _ <| |___
 *                             \___|\___/|_| \_\_____|
 *
 * Copyright (C) 1998 - 2015, Daniel Stenberg, <daniel@haxx.se>, et al.
 *
 * This software is licensed as described in the file COPYING, which you should have received as part of this distribution. The terms are also available at https:curl.haxx.se/docs/copyright.html.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell copies of the Software, and permit persons to whom the Software is furnished to do so, under the terms of the COPYING file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *
 ***************************************************************************/
/* <DESC> */
/* Shows how the write callback function can be used to download data into a */
/* chunk of memory instead of storing it in a file. */
/* </DESC> */

/* Alexander Meyer */
/* https://github.com/sigabrt/slurp/blob/master/slurp.c   */
/* Public domain, do what you want with it, though I'd certainly be interested to hear what you're using it for. :) */

/* Yaroslav Stavnichiy (nxjson) */
/* nxjson.h */
/* nxjson.c */
/* NXJSON is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. */

#define _GNU_SOURCE /* asprintf() */
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* asprintf() */
#include <time.h> /* strftime() */
#include <unistd.h> /* getopt() */
#include <oauth.h>
#include <curl/curl.h>
#include "nxjson.h"
#include "helpers.h" /* xmalloc() from libc documentation */
#include <mysql/mysql.h>

#define MS_TO_NS (1000000)

#define DATA_TIMEOUT (90)
#define BASE_URL = https://api.twitter.com/1.1/followers/ids.json

char * mysql_secrets_file_name;

/* write callback by Stenberg START */
struct MemoryStruct {
  char *memory;
  size_t size;
};
/* write callback by Stenberg STOP */

/* Meyer START */
/* read_auth_keys
 * filename: The name of the text file containing
 * 		the keys
 * bufsize: The maximum number of characters to read per line
 * ckey: Consumer key, must be allocated already
 * csecret: Consumer secret, must be allocated already
 * atok: App token, must be allocated already
 * atoksecret: App token secret, must be allocated already
 */
void read_auth_keys(const char *filename, int bufsize,
		char *ckey, char *csecret, char *atok, char *atoksecret)
{
	FILE *file = fopen(filename, "r");

	if(fgets(ckey, bufsize, file) == NULL)
	{
		return;
	}
	ckey[strlen(ckey)-1] = '\0'; // Remove the newline

	if(fgets(csecret, bufsize, file) == NULL)
	{
		return;
	}
	csecret[strlen(csecret)-1] = '\0';

	if(fgets(atok, bufsize, file) == NULL)
	{
		return;
	}
	atok[strlen(atok)-1] = '\0';

	if(fgets(atoksecret, bufsize, file) == NULL)
	{
		return;
	}
	atoksecret[strlen(atoksecret)-1] = '\0';

	fclose(file);
}
/* Meyer STOP */

/* void process_data(struct MemoryStruct * mem) { */

/*   /\* get mysql credentials *\/ */
/*   const int bufsize = 64; */
/*   char *my_user, *my_passwd, *my_host, *my_database; */
/*   my_user = (char *)malloc(bufsize * sizeof(char)); */
/*   my_passwd = (char *)malloc(bufsize * sizeof(char)); */
/*   my_host = (char *)malloc(bufsize * sizeof(char)); */
/*   my_database = (char *)malloc(bufsize * sizeof(char)); */

/*   /\* We reuse read_auth_keys() which was originally written for the four elements needed for oauth *\/ */
/*   read_auth_keys(mysql_secrets_file_name, bufsize, my_user, my_passwd, my_host, my_database); */
/*   if(my_user == NULL || my_passwd == NULL || my_host == NULL || my_database == NULL) { */
/*       fprintf(stderr, "Couldn't read mysql secrets file. Aborting...\n"); */
/*       free(my_user); */
/*       free(my_passwd); */
/*       free(my_host); */
/*       free(my_database); */
/*       exit(EXIT_FAILURE); */
/*   } */

/*   /\* We need a mysql_handle in order to get access to the function mysql_real_escape_string(). *\/ */
/*   MYSQL * mysql_handle = mysql_real_connect(mysql_init(NULL), */
/* 					    my_host, */
/* 					    my_user, */
/* 					    my_passwd, */
/* 					    my_database, */
/* 					    0, */
/* 					    "/var/run/mysqld/mysqld.sock", */
/* 					    0); */

/*   int my_set_charset_results = mysql_set_character_set(mysql_handle, "utf8mb4"); */
/*   if(my_set_charset_results > 0){ */
/*     printf("Failed to set the character set of the connection to utf8mb4\n"); */
/*     exit(EXIT_FAILURE); */
/*   } */

/*   printf("data: %s", mem->memory); */
/*   /\* For now, just save the data *\/ */
/*   FILE * fh = fopen("my_data.txt", "a"); */
/*   if(fh != NULL){ */
/*     fputs(mem->memory, fh); */
/*     fclose(fh); */
/*   } else { */
/*     printf("Failed to open error log, giving up...\n"); */
/*     exit(EXIT_FAILURE); */
/*   } */

/*   mysql_close(mysql_handle); */
/*   free(my_user); */
/*   free(my_passwd); */
/*   free(my_host); */
/*   free(my_database); */
  
/*   /\* We are ok, return void *\/ */
/*   return; */
/* } */

/* write callback by Stenberg START */

static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;

  /* printf("realloc %lu \n", (long)size+realsize+1); */

  mem->memory = realloc(mem->memory, mem->size + realsize + 1);
  if(mem->memory == NULL) {
    /* out of memory! */
    printf("not enough memory (realloc returned NULL)\n");
    return 0;
  }

  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;

  return realsize;
}
/* write callback by Stenberg STOP */

/* This part by Meyer START */
typedef enum
{
	ERROR_TYPE_HTTP,
	ERROR_TYPE_RATE_LIMITED,
	ERROR_TYPE_SOCKET,
} error_type;

struct idletimer
{
	int lastdl;
	time_t idlestart;
};

/* write callback by Stenberg START */
int progress_callback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow);
struct MemoryStruct chunk;
/* write callback by Stenberg STOP */

/* config_curlopts
 * curl: cURL easy handle
 * url: URL of streaming endpoint
 * postargs: POST parameters
 * outfile: file stream for retrieved data
 * prog_data: data to send to progress callback function
 */

void config_curlopts(CURL *curl, const char *url, char *postargs, void *prog_data)
{
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "tweetslurp/0.1");

	/* use a GET to fetch this */
	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);

	// libcurl will now fail on an HTTP error (>=400)
	curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);

	/* write callback by Stenberg START */
	/* send all data to this function  */
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
	/* we pass our 'chunk' struct to the callback function */
	/* curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk); */
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);	
	/* write callback by Stenberg STOP */

	// noprogress must be set to 0 for a user-defined
	// progress method to be called
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);

	curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, progress_callback);
	curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, (void *)prog_data);
}



int main(int argc, char *argv[])
{

  /* write callback by Stenberg START */
  chunk.memory = malloc(1);  /* will be grown as needed by the realloc above */
  chunk.size = 0;    /* no data at this point */
  /* write callback by Stenberg STOP */

  /* This part by Meyer START */  
  /* File containing the OAuth keys */
  const char *keyfile = NULL;

  /* OAuth keys */
  char *ckey, *csecret, *atok, *atoksecret;

  /* Signed URL */
  char *signedurl = NULL;

  keyfile = argv[1];

  /* File containing the mysql username, password, host, and table */
  mysql_secrets_file_name = argv[2];

  static const char *myurl =
	  "https://api.twitter.com/1.1/followers/ids.json?screen_name=LennartBucht&skip_status=true&include_user_entities=false";
	
  printf("url: %s \n", myurl);

  // Read the OAuth keys
  // ===================
  // These may be found on your twitter dev page, under "Applications"
  // You will need to create a new app if you haven't already
  // The four keys should be on separate lines in this order:
  int bufsize = 64;
  ckey = (char *)malloc(bufsize * sizeof(char));
  csecret = (char *)malloc(bufsize * sizeof(char));
  atok = (char *)malloc(bufsize * sizeof(char));
  atoksecret = (char *)malloc(bufsize * sizeof(char));
  read_auth_keys(keyfile, bufsize, ckey, csecret, atok, atoksecret);
  if(ckey == NULL || csecret == NULL ||
     atok == NULL || atoksecret == NULL)
    {
      fprintf(stderr, "Couldn't read key file. Aborting...\n");
      free(ckey);
      free(csecret);
      free(atok);
      free(atoksecret);
      return 1;
    }


  // Sign the URL with OAuth
  // =======================
  // Note that all parameters will be stripped from the URL
  // and added to postargs automatically
  signedurl = oauth_sign_url2(myurl, NULL, OA_HMAC, "GET", ckey, csecret, atok, atoksecret);

  printf("signedurl: %s \n", signedurl);

  // Initialize the idle timer
  // =========================
  struct idletimer timeout;
  timeout.lastdl = 1;
  timeout.idlestart = 0;

  // Initialize libcURL
  // ==================
  curl_global_init(CURL_GLOBAL_ALL);
  CURL *curl = curl_easy_init();
  config_curlopts(curl, signedurl, NULL, (void *)&timeout);

  int curlstatus = curl_easy_perform(curl);
  if(curlstatus != 0){
    printf("curl_easy_perform returned error %d\n", curlstatus);
  }

  const nx_json* json=nx_json_parse_utf8(chunk.memory);
  if(json){
      const nx_json* arr=nx_json_get(json, "ids");
      int i;
      for (i=0; i<arr->length; i++) {
	const nx_json* item=nx_json_item(arr, i);
	printf("%lli, ", item->int_value);
      }
      nx_json_free(json);
  } else {
    printf("failed to parse %s", chunk.memory);
  }
    
  curl_easy_cleanup(curl);
  free(chunk.memory);
  curl_global_cleanup();
  
  free(ckey);
  free(csecret);
  free(atok);
  free(atoksecret);
  free(signedurl);

  return 0;
}

/* progress_callback
 * see libcURL docs for method sig details
 */
int progress_callback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow)
{
  struct idletimer *timeout;
  timeout = (struct idletimer *)clientp;
  
  if(dlnow == 0) // No data was transferred this time...
    {
      // ...but some was last time:
      if(timeout->lastdl != 0)
	{
	  // so start the timer
	  timeout->idlestart = time(NULL);
	}
      // ...and 1) the timer has been started, and
      // 2) we've hit the timeout:
      else if(timeout->idlestart != 0 &&
	      (time(NULL) - timeout->idlestart) > DATA_TIMEOUT)
	{
	  // so we reset the timer and return a non-zero
	  // value to abort the transfer
	  timeout->lastdl = 1;
	  timeout->idlestart = 0;
	  return 1;
	}
    }
  else // We transferred some data...
    {
      // ...but we didn't last time:
      if(timeout->lastdl == 0)
	{
	  // so reset the timer
	  timeout->idlestart = 0;
	}
    }
  
  timeout->lastdl = dlnow;
  return 0;
}
